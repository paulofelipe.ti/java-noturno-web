/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.treinacom.model;

/**
 *
 * @author Aluno
 */
public class Curso {    
    private int id;
    private String nome;
    private int cargaHoraria;
    
    public void curso(){
    
    }
    
    public void curso(int id, String nome, int cargaHoraria){
        this.id = id;
        this.nome = nome;
        this.cargaHoraria = cargaHoraria;
    
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(int cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }
    
}
