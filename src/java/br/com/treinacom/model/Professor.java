/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.treinacom.model;

/**
 *
 * @author Aluno
 */
public class Professor {    
    private int id;
    private String nome;
    private String formacao;
        
    public void professor(){
    
    }
    
    public void professor(int id, String nome, String formacao){
        this.id = id;
        this.nome = nome;
        this.formacao = formacao;             
    
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFormacao() {
        return formacao;
    }

    public void setFormacao(String formacao) {
        this.formacao = formacao;
    }
    
}
