/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.treinacom.dao;

import br.com.treinacom.model.Aluno;
import br.com.treinacom.repositorio.CrudDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Aluno
 */
public class AlunoDAO implements CrudDAO<Aluno>{

    @Override
    public void gravar(Aluno aluno) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "INSERT INTO Aluno (nome, cpf, matricula, endereco) VALUES (?, ?, ?, ?)";
        PreparedStatement stmt;
        stmt = con.prepareStatement(sql);
        stmt.setString(1, aluno.getNome());
        stmt.setString(2, aluno.getCpf());
        stmt.setString(3, aluno.getMatricula());
        stmt.setString(4, aluno.getEndereco());
        stmt.execute();
        stmt.close();
        con.close(); 
    }

    @Override
    public void editar(Aluno aluno) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "UPDATE Aluno SET nome = ?, cpf = ?, matricula = ?, endereco = ? WHERE id = ?";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setString(1, aluno.getNome());
        stmt.setString(2, aluno.getCpf());
        stmt.setString(3, aluno.getMatricula());
        stmt.setString(4, aluno.getEndereco());
        stmt.setInt(5, aluno.getId());
        stmt.execute();
        stmt.close();
        con.close();
        
    }

    @Override
    public void remover(int id) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "DELETE FROM Aluno WHERE id = ? ";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, id);
        stmt.execute();
        stmt.close();
        con.close();
        
    }

    @Override
    public List<Aluno> listar() throws SQLException{
         
            Connection con = Conexao.getConnection();
            String sql = "SELECT id, nome, cpf, matricula, endereco FROM Aluno";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            
            List<Aluno> alunos = new ArrayList();
            
            while(rs.next()){
                Aluno aluno = new Aluno();
                aluno.setId(rs.getInt("id"));
                aluno.setNome(rs.getString("nome"));
                aluno.setCpf(rs.getString("cpf"));
                aluno.setMatricula(rs.getString("matricula"));   
                aluno.setEndereco(rs.getString("endereco"));
                alunos.add(aluno);            
                                
            }
            
            return alunos;        
        
        }

    @Override
    public Aluno pesquisar(int id) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "SELECT id, nome, cpf, matricula, endereco FROM Aluno WHERE id = ?";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        rs.next();
        Aluno aluno = new Aluno();
        aluno.setId(rs.getInt(1));
        aluno.setNome(rs.getString(2));
        aluno.setCpf(rs.getString(3));
        aluno.setMatricula(rs.getString(4));
        aluno.setEndereco(rs.getString(5));
        return aluno;
    }

    @Override
    public List<Aluno> filtrar(Aluno aluno) throws SQLException {
        Connection conn = Conexao.getConnection();
        String sql = "SELECT * FROM Aluno WHERE nome like ? or cpf = ? or matricula = ?";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, aluno.getNome());
        stmt.setString(2, aluno.getCpf());
        stmt.setString(3, aluno.getMatricula());
        
        ResultSet rs = stmt.executeQuery();
        List<Aluno> alunos = new ArrayList();
        while(rs.next()){
            Aluno a = new Aluno();
            
            a.setId(rs.getInt(1));
            a.setNome(rs.getString(2));
            a.setCpf(rs.getString(3));
            a.setMatricula(rs.getString(4));
            alunos.add(a);
        }
        
        return alunos;
    }
    
}
