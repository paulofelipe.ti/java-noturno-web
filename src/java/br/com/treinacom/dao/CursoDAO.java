/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.treinacom.dao;

import br.com.treinacom.model.Curso;
import br.com.treinacom.model.Professor;
import br.com.treinacom.repositorio.CrudDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class CursoDAO implements CrudDAO<Curso>{
    
    @Override
    public void gravar(Curso curso) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "INSERT INTO Curso (nome, cargaHoraria) VALUES (?, ?)";
        PreparedStatement stmt;
        stmt = con.prepareStatement(sql);
        stmt.setString(1, curso.getNome());
        stmt.setInt(2, curso.getCargaHoraria());
        stmt.execute();
        stmt.close();
        con.close();        
    }

    @Override
    public void editar(Curso curso) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "UPDATE Curso SET nome = ?, cargaHoraria = ? WHERE id = ?";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setString(1, curso.getNome());
        stmt.setInt(2, curso.getCargaHoraria());
        stmt.setInt(3, curso.getId());
        stmt.execute();
        stmt.close();
        con.close();
    }   
        

    @Override
    public void remover(int id) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "DELETE FROM Curso WHERE id = ? ";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, id);
        stmt.execute();
        stmt.close();
        con.close();        
    }

    @Override
    public List<Curso> listar() {
         try{
            Connection con = Conexao.getConnection();
            String sql = "SELECT id, nome, cargaHoraria FROM Curso";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            
            List<Curso> cursos = new ArrayList();
            
            while(rs.next()){
                Curso curso = new Curso();
                curso.setId(rs.getInt("id"));
                curso.setNome(rs.getString("nome"));
                curso.setCargaHoraria(rs.getInt("cargaHoraria"));
                cursos.add(curso);            
                                
            }
            
            return cursos;
        
        }catch(SQLException ex){
        return null;
        }
    }

    @Override
    public Curso pesquisar(int id) throws SQLException {
        return null;
    }

    @Override
    public List<Curso> filtrar(Curso curso) throws SQLException {
        return null;
    } 
    
}
