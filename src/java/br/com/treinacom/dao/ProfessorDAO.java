/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.treinacom.dao;

import br.com.treinacom.model.Professor;
import br.com.treinacom.repositorio.CrudDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class ProfessorDAO implements CrudDAO<Professor>{
    
    @Override
    public void gravar(Professor professor) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "INSERT INTO Professor (nome, formacao) VALUES (?, ?)";
        PreparedStatement stmt;
        stmt = con.prepareStatement(sql);
        stmt.setString(1, professor.getNome());
        stmt.setString(2, professor.getFormacao());
        stmt.execute();
        stmt.close();
        con.close();
        
    }

    @Override
    public void editar(Professor professor) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "UPDATE Professor SET nome = ?, formacao = ? WHERE id = ?";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setString(1, professor.getNome());
        stmt.setString(2, professor.getFormacao());
        stmt.setInt(3, professor.getId());
        stmt.execute();
        stmt.close();
        con.close();
        
    }

    @Override
    public void remover(int id) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "DELETE FROM Professor WHERE id = ? ";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, id);
        stmt.execute();
        stmt.close();
        con.close(); 
        
    }

    @Override
    public List<Professor> listar(){
        try{
            Connection con = Conexao.getConnection();
            String sql = "SELECT id, nome, formacao FROM Professor";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            
            List<Professor> professores = new ArrayList();
            
            while(rs.next()){
                Professor professor = new Professor();
                professor.setId(rs.getInt("id"));
                professor.setNome(rs.getString("nome"));
                professor.setFormacao(rs.getString("formacao"));
                professores.add(professor);            
                                
            }
            
            return professores;
        
        }catch(SQLException ex){
        return null;
        }
    }

    @Override
    public Professor pesquisar(int id) throws SQLException {
        return null;
    }

    @Override
    public List<Professor> filtrar(Professor professor) throws SQLException {
        return null;
    }    
    
}
