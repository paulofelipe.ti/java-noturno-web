/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.treinacom.dao;

import br.com.treinacom.model.Aluno;
import br.com.treinacom.repositorio.CrudDAO;
import br.com.treinacom.model.AlunoCurso;
import br.com.treinacom.model.Curso;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AlunoCursoDAO implements CrudDAO<AlunoCurso> {
    
    AlunoCurso ac = new AlunoCurso();

    @Override
    public void gravar(AlunoCurso ac) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "INSERT INTO (idAluno, idCurso) VALUES (?, ?)";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, ac.getIdAluno());
        stmt.setInt(2, ac.getIdCurso());
        stmt.executeQuery();
        stmt.close();
        con.close();
    }

    @Override
    public void editar(AlunoCurso ac) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "UPDATE alunoCurso SET idAluno = ?, idCurso = ? WHERE id = ?";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, ac.getIdAluno());
        stmt.setInt(2, ac.getIdCurso());
        stmt.setInt(3, ac.getId());
        stmt.executeQuery();
        stmt.close();
        con.close();
    }

    @Override
    public void remover(int id) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "DELETE FROM alunoCurso WHERE id = ?";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, id);
        stmt.close();
        con.close();
    }

    @Override
    public List<AlunoCurso> listar() throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "SELECT ac.id, ac.idAluno, ac.idCurso, a.nome, c.nome FROM alunocurso as ac INNER JOIN aluno as a ON ac.IdAluno = a.id INNER JOIN curso as c ON ac.IdCurso = c.Id; ";
        PreparedStatement stmt = con.prepareStatement(sql);
        
        ResultSet rs = stmt.executeQuery();
        
        List<AlunoCurso> acursos = new ArrayList();
        while(rs.next()){
            AlunoCurso ac = new AlunoCurso();
            ac.setId(rs.getInt(1));
            ac.setIdAluno(rs.getInt(2));
            ac.setIdCurso(rs.getInt(3));
            ac.setAlunoNome(rs.getString(4));
            ac.setCursoNome(rs.getString(5));
            acursos.add(ac);
            
        }
        
        return acursos;
    }

    @Override
    public AlunoCurso pesquisar(int id) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "SELECT id, alunoId, cursoId FROM alunocurso WHERE id = ?;";
        
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, id);        
        ResultSet rs = stmt.executeQuery();
        rs.next();        
        AlunoCurso ac = new AlunoCurso();
        ac.getId();
        return ac;
    }

    @Override
    public List<AlunoCurso> filtrar(AlunoCurso ac) throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "SELECT ac.id, ac.idAluno, ac.idCurso, a.nome, c.nome FROM alunocurso as ac INNER JOIN aluno as a ON ac.IdAluno = a.id INNER JOIN curso as c ON ac.IdCurso = c.Id;";
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, ac.getIdAluno());
        stmt.setInt(2, ac.getIdCurso());
        
        ResultSet rs = stmt.executeQuery();
        List<AlunoCurso> acursos = new ArrayList();
        while(rs.next()){
            AlunoCurso acurso = new AlunoCurso();
            acurso.setId(rs.getInt(1));
            acurso.setIdAluno(rs.getInt(2));
            acurso.setIdCurso(rs.getInt(3));
            acurso.setAlunoNome(rs.getString(4));
            acurso.setCursoNome(rs.getString(5));
            acursos.add(acurso);
        }
        
        return acursos;
    }
    
    public List<Aluno> alunos() throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "SELECT id, nome FROM Aluno";
        PreparedStatement stmt = con.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();
        
        List<Aluno> alunos = new ArrayList();
        while(rs.next()){
            Aluno aluno = new Aluno();
            aluno.setId(rs.getInt(1));
            aluno.setNome(rs.getString(2));
            alunos.add(aluno);
        }
        
        return alunos;
    }
    
    public List<Curso> cursos() throws SQLException {
        Connection con = Conexao.getConnection();
        String sql = "SELECT id, nome FROM Curso";
        PreparedStatement stmt = con.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();
        
        List<Curso> cursos = new ArrayList();
        while(rs.next()){
            Curso curso = new Curso();
            curso.setId(rs.getInt(1));
            curso.setNome(rs.getString(2));
            cursos.add(curso);
        }
        
        return cursos;
    }
}
