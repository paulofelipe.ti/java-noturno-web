/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.treinacom.repositorio;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Paulo Felipe
 * @param <T>
 */
public interface CrudDAO <T>{
    public void gravar(T objeto) throws SQLException;
    public void editar(T objeto) throws SQLException;
    public void remover(int id) throws SQLException;
    public List<T> listar() throws SQLException;
    public T pesquisar(int id) throws SQLException;
    public List<T> filtrar(T objeto) throws SQLException;
    
}
