/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.treinacom.servlet;

import br.com.treinacom.dao.AlunoDAO;
import br.com.treinacom.model.Aluno;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CadastroAlunoServlet")

public class CadastroAlunoServlet extends HttpServlet{
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        
        AlunoDAO alunoDAO = new AlunoDAO();
        Aluno aluno = new Aluno(0, "", "", "", "");
        
        if(request.getParameter("id") != null){
            try {
                aluno = alunoDAO.pesquisar(Integer.parseInt(request.getParameter("id")));
            } catch(SQLException ex) {
                Logger.getLogger(CadastroAlunoServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        request.setAttribute("aluno", aluno);        
        request.getRequestDispatcher("/WEB-INF/alunos/cadastro.jsp").forward(request, response);
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        
        Aluno aluno = new Aluno();
        aluno.setNome(request.getParameter("txNome"));
        aluno.setCpf(request.getParameter("txCpf"));
        aluno.setMatricula(request.getParameter("txMatricula"));
        aluno.setEndereco(request.getParameter("txEndereco"));
        
        AlunoDAO alunoDAO = new AlunoDAO();
        String id = request.getParameter("txId");
        
        try{
            if (id.equals("0")) {
                alunoDAO.gravar(aluno);
            }else {
                aluno.setId(Integer.parseInt(id));
                alunoDAO.editar(aluno);
            }           
        }catch(SQLException ex){
           ex.getMessage();
        }
        
        response.sendRedirect("./AlunoServlet");
    }
    
}
