
package br.com.treinacom.servlet;

import br.com.treinacom.dao.AlunoCursoDAO;
import br.com.treinacom.model.Aluno;
import br.com.treinacom.model.AlunoCurso;
import br.com.treinacom.model.Curso;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/AlunoCursoServlet")
public class AlunoCursoServlet extends HttpServlet{
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        AlunoCursoDAO acDAO = new AlunoCursoDAO();
                
        try{
            List<AlunoCurso> alunoCursos = acDAO.listar();
            List<Aluno> alunos = acDAO.alunos();  
            List<Curso> cursos = acDAO.cursos();
            request.setAttribute("alunoCursos", alunoCursos);
            request.setAttribute("alunos", alunos);
            request.setAttribute("cursos", cursos);
            
            request.getRequestDispatcher("./WEB-INF/matricula/lista.jsp").forward(request, response);
            
        }catch(SQLException ex){
            
        }
    }    
}
