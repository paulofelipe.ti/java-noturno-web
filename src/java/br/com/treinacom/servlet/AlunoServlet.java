/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.treinacom.servlet;

import br.com.treinacom.dao.AlunoDAO;
import br.com.treinacom.model.Aluno;
import java.io.IOException;
import static java.lang.System.out;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/AlunoServlet")

public class AlunoServlet extends HttpServlet{
    
        
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        AlunoDAO alunoDAO = new AlunoDAO();
        
        try{
            if(request.getParameter("o") != null){
                alunoDAO.remover(Integer.parseInt(request.getParameter("id")));
            }
        }catch(SQLException ex){
            out.println(ex.getMessage());
        }
                
       try{
        List<Aluno> alunos;
            if(request.getParameter("txNome") == null){
                alunos = alunoDAO.listar();
            }else{
                Aluno a = new Aluno();
                a.setNome(request.getParameter("txNome"));
                a.setCpf(request.getParameter("txCpf"));
                a.setMatricula(request.getParameter("txMatricula"));
                alunos = alunoDAO.filtrar(a);
            }
        //Criando uma variável para enviar os alunos para o JSP
        request.setAttribute("alunos", alunos);
        
       }catch(SQLException ex){
           out.println(ex.getMessage());
       }
        //Carregando a página lista.jsp da pasta alunos
        request.getRequestDispatcher("/WEB-INF/alunos/lista.jsp").forward(request, response);
        
    }   
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        
        Aluno aluno = new Aluno();
        aluno.setNome(request.getParameter("txNome"));
        aluno.setCpf(request.getParameter("txCpf"));
        aluno.setMatricula(request.getParameter("txMatricula"));
        aluno.setEndereco(request.getParameter("txEndereco"));
        
        AlunoDAO alunoDAO = new AlunoDAO();
        try{
            alunoDAO.gravar(aluno);
        }catch(SQLException ex){
            //Tratamento de exceção
        }
        
        response.sendRedirect("./AlunoServlet");
    }
}
 
