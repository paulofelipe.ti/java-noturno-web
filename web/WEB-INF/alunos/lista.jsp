<%@page import="br.com.treinacom.model.Aluno"%>
<%@page import="br.com.treinacom.dao.AlunoDAO"%>
<%@page import="java.util.List" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
  //  AlunoDAO alunoDAO = new AlunoDAO();
  // List<Aluno> alunos = alunoDAO.listar();
  
    List<Aluno> alunos = (List) request.getAttribute("alunos");
%>
<%@include file="../navbar.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Gestão Escola</title>
        <script>
            function remover(id){
                if(confirm("Deseja realmente remover?")){
                    document.location = 'AlunoServlet?o=d&id=' + id;
                }
            }
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3>Lista de Alunos</h3>
            <br>
            <div class="card">
                <div class="card-header">
                    Filtro
                </div>
                <div class="card-body">
                    <form action="./AlunoServlet">
                        <div class="row">
                            <div class="col-md">
                                <label>Nome:</label>
                                <input name="txNome" class="form-control" />
                            </div>
                            <div class="col-md">
                                <label>CPF:</label>
                                <input name="txCpf" class="form-control" />
                            </div>
                            <div class="col-md">
                                <label>Matrícula:</label>
                                <input name="txMatricula" class="form-control" />
                            </div>
                            <div class="col-md-2" style="margin-top: 30px;">
                                <button type="submit" style="width: 100%" class="btn btn-primary">Filtrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br />
            <button class="btn btn-primary" onclick="document.location='CadastroAlunoServlet'">Novo Aluno</button>
            <br>
            <br>
            <table class="table table-bordered table-striped">               
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Matrícula</th>
                        <th style="width: 190px">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <% for(int i = 0; i < alunos.size(); i++) {%>
                <tr>
                    <td><%=alunos.get(i).getId()%></td>
                    <td><%=alunos.get(i).getNome()%></td>
                    <td><%=alunos.get(i).getCpf()%></td>
                    <td><%=alunos.get(i).getMatricula()%></td>
                    <td>
                        <button class="btn btn-primary" onclick="document.location='CadastroAlunoServlet?id=' + <%=alunos.get(i).getId()%>">Editar</button>
                        <button class="btn btn-danger" onclick="remover(<%=alunos.get(i).getId()%>)">Remover</button>
                    </td>
                </tr>   
                     <% } %>
               </tbody>
            </table>
        </div>
        
    </body>
</html>
