<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!--<link rel="shortcut icon" href="caminhodoarquivo/favicon.ico" />-->
                 
        <div class="-fluid">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="./index.html">Gest�o Escolar</a>
                 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                 </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="./AlunoServlet">Alunos<span class="sr-only">(current)</span></a>
                  </li>                 
                  <li class="nav-item">
                    <a class="nav-link" href="./CursoServlet">Cursos</a>
                  </li>     
                  <li class="nav-item">
                    <a class="nav-link" href="./ProfessorServlet">Professores</a>
                  </li>   
                  <li class="nav-item">
                    <a class="nav-link" href="./AlunoCursoServlet">Matr�culas</a>
                  </li>   
                </ul>
             
          </div>
        </nav>
        </div>
