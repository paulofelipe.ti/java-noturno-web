-- Comentário de uma linha
/*Comentário de mais de uma linha*/

# Criando o banco de dados
CREATE DATABASE GestaoEscola;

# Remover o banco de dados
DROP DATABASE GestaoEscola;

#Selecionando o banco de dados
USE GestaoEscola;

#Criando a tabela Aluno
CREATE TABLE Aluno(
	id INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(50) NOT NULL,
    cpf CHAR(14) NOT NULL,
    matricula CHAR(6) NULL
);

#Criar a tabela Curso com as colunas
#id sequencial e chave da tabela
#nome alfanumérico de no máximo 30 caracteres
#cargaHoraria numérica

CREATE TABLE Curso(
	id INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(30) NOT NULL,
    cargaHoraria INT NULL
);


CREATE TABLE AlunoCurso(
	id INT AUTO_INCREMENT PRIMARY KEY,
	alunoID INT NOT NULL,
    cursoID INT NOT NULL,
    CONSTRAINT fkAlunoCurso FOREIGN KEY(alunoID) REFERENCES Aluno(id),
	CONSTRAINT fkCursoAluno FOREIGN KEY(cursoID) REFERENCES Curso(id)
);

#Criar a tabela Professor com as colunas
#id 
#nome
#formacao

#Criar a tabela ternaria de relacionamento entre 
#a tabela de professor e curso
CREATE TABLE Professor(
	id INT AUTO_INCREMENT,
    nome VARCHAR(30) NOT NULL,
    formacao VARCHAR(40) DEFAULT 'N/A',
    CONSTRAINT pkProfessor PRIMARY KEY(id)
);

CREATE TABLE ProfessorCurso(
	id INT AUTO_INCREMENT PRIMARY KEY,
    professorID INT NOT NULL,
    cursoID INT NOT NULL,
    FOREIGN KEY(professorID) REFERENCES Professor(id),
    FOREIGN KEY(cursoID) REFERENCES Curso(id)
);












