<%@page import="br.com.treinacom.model.Aluno" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    //Recebendo variável do Servlet
    Aluno aluno = (Aluno) request.getAttribute("aluno");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Gestão Escolar</title>
    </head>
    <body>
        <div class="container-fluid"> 
            <br />
            <div class="card border-primary">
            <div class="card-header">
               Cadastro de Aluno
            </div>
                <div class="card-body">
                    <form action="./CadastroAlunoServlet" method="post">
                        <input type="hidden" name="txId" value="<%=aluno.getId()%>" />
                        <div class="row">
                        <!--Formulário -->
                            <div class="col-md">
                                <label for="txNome">Nome:</label>
                                <input name="txNome" id="txNome" class="form-control" value="<%=aluno.getNome()%>"/>
                            </div>
                    
                            <div class="col-md">
                                <label for="txCpf">CPF:</label>
                                <input name="txCpf" id="txCpf" class="form-control" value="<%=aluno.getCpf()%>" />
                            </div>
                    
                            <div class="col-md">
                                <label for="txMatricula">Matrícula</label>
                                <input name="txMatricula" id="txMatricula" class="form-control" value="<%=aluno.getMatricula()%>"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <label for="txEndereco">Endereço</label>
                                <input name="txEndereco" id="txEndereco" class="form-control" value="<%=aluno.getEndereco()%>"/>
                            </div>
                    
                            <div class="col-md-2" style="margin-top: 30px;">
                                <button type="submit" style="width:100%;" class="btn btn-success">Gravar</button> 
                            </div>
                        </div>                      
                    </form>
                </div>
            </div>
        </div>   
   </body>
</html>
