<%@page import="br.com.treinacom.model.Curso"%>
<%@page import="br.com.treinacom.model.Aluno"%>
<%@page import="br.com.treinacom.model.AlunoCurso"%>
<%@page import="br.com.treinacom.dao.AlunoCursoDAO"%>
<%@page import="java.util.List" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
  //  AlunoDAO alunoDAO = new AlunoDAO();
  // List<Aluno> alunos = alunoDAO.listar();
  
    List<AlunoCurso> alunoCursos = (List) request.getAttribute("alunoCursos");
    List<Aluno> alunos = (List) request.getAttribute("alunos");
    List<Curso> cursos = (List) request.getAttribute("cursos");
    
%>
<%@include file="../navbar.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Gestão Escola</title>
        <script>
            function remover(id){
                if(confirm("Deseja realmente remover?")){
                    document.location = 'AlunoCursoServlet?o=d&id=' + id;
                }
            }
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <h3>Lista de Matrículas</h3>
            <br>
            <div class="card">
                <div class="card-header">
                    Filtro
                </div>
                <div class="card-body">
                    <form action="./AlunoCursoServlet">
                        <div class="row">
                            <div class="col-md">
                                <label>Aluno</label>
                                <select class="form-control" name="cbAluno">
                                    <option value="0">Todos</option>
                                    <% for(int i = 0; i < alunos.size(); i++) {%>
                                    <option value="<%=alunos.get(i).getId()%>"><%=alunos.get(i).getNome()%></option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="col-md">
                                <label>Curso</label>
                                <select class="form-control" name="cbCurso">
                                    <option value="0">Todos</option>
                                    <% for(int i = 0; i < cursos.size(); i++) {%>
                                    <option value="<%=cursos.get(i).getId()%>"><%=cursos.get(i).getNome()%></option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="col-md">
                                <label>Matrícula:</label>
                                <input name="txMatricula" class="form-control" />
                            </div>
                            <div class="col-md-2" style="margin-top: 30px;">
                                <button type="submit" style="width: 100%" class="btn btn-primary">Filtrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br />
            <button class="btn btn-primary" onclick="document.location='CadastroAlunoCursoServlet'">Nova Matrícula</button>
            <br>
            <br>
            <table class="table table-bordered table-striped">               
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nome Aluno</th>
                        <th>Nome Curso</th>
                       
                        <th style="width: 190px">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <% for(int i = 0; i < alunoCursos.size(); i++) {%>
                <tr>
                    <td><%=alunoCursos.get(i).getId()%></td>
                    <td><%=alunoCursos.get(i).getAlunoNome()%></td>
                    <td><%=alunoCursos.get(i).getCursoNome()%></td>
                    
                    <td>
                        <button class="btn btn-primary" onclick="document.location='CadastroAlunoCursoServlet?id=' + <%=alunoCursos.get(i).getId()%>">Editar</button>
                        <button class="btn btn-danger" onclick="remover(<%=alunoCursos.get(i).getId()%>)">Remover</button>
                    </td>
                </tr>   
                     <% } %>
               </tbody>
            </table>
        </div>
        
    </body>
</html>