<%@page import="br.com.treinacom.model.Curso" %>
<%@page import="br.com.treinacom.dao.CursoDAO" %>
<%@page import="java.util.List" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    CursoDAO cursoDAO = new CursoDAO();
    List<Curso> cursos = cursoDAO.listar();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Gestão Escola</title>
    </head>
    <body>
        <div class="container-fluid">
            <h3>Lista de Cursos</h3>
            <br>
            <button class="btn btn-primary">Novo Curso</button>
            <br>
            <br>
            <table class="table table-bordered table-striped">               
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nome</th>
                        <th>Carga Horária</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <% for(int i = 0; i < cursos.size(); i++) {%>
                <tr>
                    <td><%=cursos.get(i).getId()%></td>
                    <td><%=cursos.get(i).getNome()%></td>
                    <td><%=cursos.get(i).getCargaHoraria()%></td>
                    <td>Ações</td>
                </tr>   
                     <% } %>
               </tbody>
            </table>
        </div>
        
    </body>
</html>
