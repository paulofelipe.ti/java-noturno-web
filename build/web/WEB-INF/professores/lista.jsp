<%@page import="br.com.treinacom.model.Professor" %>
<%@page import="br.com.treinacom.dao.ProfessorDAO" %>
<%@page import="java.util.List" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    ProfessorDAO professorDAO = new ProfessorDAO();
    List<Professor> professores = professorDAO.listar();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Gestão Escola</title>
    </head>
    <body>
        <div class="container-fluid">
            <h3>Lista de Professores</h3>
            <br>
            <button class="btn btn-primary">Novo Professor</button>
            <br>
            <br>
            <table class="table table-bordered table-striped">               
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nome</th>
                        <th>Formação</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <% for(int i = 0; i < professores.size(); i++) {%>
                <tr>
                    <td><%=professores.get(i).getId()%></td>
                    <td><%=professores.get(i).getNome()%></td>
                    <td><%=professores.get(i).getFormacao()%></td>
                    <td>Ações</td>
                </tr>   
                     <% } %>
               </tbody>
            </table>
        </div>
        
    </body>
</html>

